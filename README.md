# CARA Mobile and Desktop Apps

These are the files needed to run the mobile and desktop applications for CARA

To run these applications the CARA sofware must be running on a local server at http://localhost:8080. Once you follow the instructions at https://gitlab.cern.ch/cara/cara it will automatically be hosting the website at the proper address.

**Desktop App**

To run the CARA desktop app, go inside the desktopApp folder and run 

`npm install`

and then to start the app run

`npm start`



**Mobile App**

To run the CARA mobile app, again enter the mobileApp folder and run 

`yarn install`

This will install all dependencies. If it does not work fully you can try running 

`yarn add react-native-webview `

and then running the install again.

Now run 

`npm start`

and you will be brought to a webpage where you can chose how to run the application either through a simulator or on your own device.
